package main

import (
	"math/rand"
	"fmt"
	"sort"
)

func random(min, max int) int {
	return rand.Intn(max - min) + min
}

func check_sorting(list []int) bool {
	for i := 1; i < len(list); i++ {
		if list[i-1] <= list[i] {
			continue
		}
		fmt.Println(list[i-1], list[i], list[i-1] <= list[i])
		return false
	}
	return true
}

func getList(listType string, size int) []int {
	var result []int
	switch listType {
	case "liner":
		result = getLinerList(size)
	case "reversed":
		result = getLinerList(size)
		sort.Sort(sort.Reverse(sort.IntSlice(result)))
	case "degenerated":
		result = getDegeneratedList(size)
	case "random":
		result = getRandomList(size)
	}

	return result
}

func getDegeneratedElement () int {
	return random(1, 12)
}

func getRandomElement () int {
	return random(0, 7000)
}

func getDegeneratedList(size int) []int {
	list := make([]int, size, size)

	for i := 0; i < len(list); i++ {
		list[i] = getDegeneratedElement()
	}

	return list
}

func getRandomList(size int) []int {
	list := make([]int, size, size)

	for i := 0; i < len(list); i++ {
		list[i] = getRandomElement()
	}

	return list
}

func getLinerList(size int) []int {
	list := make([]int, size, size)

	for i := 0; i < len(list); i++ {
		list[i] = i
	}

	return list
}
