package main

import (
	"fmt"
	"time"
)

func main() {
	sizesOfArrays := []int { 1000, 2000, 4000, 8000, 16000, 32000, 64000, 128000, 256000, 512000, 1024000 }
	typesOfArrays := []string { "liner", "reversed", "degenerated", "random" }

	for ti := 0; ti < len(typesOfArrays); ti++ {
		for i := 0; i < len(sizesOfArrays); i++ {
			theSize := sizesOfArrays[i]
			theType := typesOfArrays[ti]

			list := getList(theType, theSize)

			start := time.Now()
			bubbleSort(list)
			fmt.Println("BubbleSorting List of", theType, theSize, "is sorted =", check_sorting(list), time.Since(start))
		}
	}

	for ti := 0; ti < len(typesOfArrays); ti++ {
		for i := 0; i < len(sizesOfArrays); i++ {
			theSize := sizesOfArrays[i]
			theType := typesOfArrays[ti]

			list := getList(theType, theSize)

			start := time.Now()
			sortedList := quickSort(list)

			fmt.Println("QuickSorting List of", theType, theSize, "is sorted =", check_sorting(sortedList), time.Since(start))
		}
	}
}
